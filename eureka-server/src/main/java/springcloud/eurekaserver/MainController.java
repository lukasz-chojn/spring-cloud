package springcloud.eurekaserver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @Value("${eureka.welcome.message}")
    private String message;

    @GetMapping("/hello")
    public String getMessage() {
        return message;
    }
}
