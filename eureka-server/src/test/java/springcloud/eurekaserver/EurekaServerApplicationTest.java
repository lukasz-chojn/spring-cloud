package springcloud.eurekaserver;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EurekaServerApplicationTest {

    private MainController mainController;

    @Before
    public void setUp() {
        mainController = new MainController();
    }

    @Test
    public void testMain() {
        ReflectionTestUtils.setField(mainController, "message", "Hello");
    }
}