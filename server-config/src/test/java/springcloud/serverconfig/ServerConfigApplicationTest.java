package springcloud.serverconfig;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.junit.Test;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ServerConfigApplication.class)
@WebAppConfiguration
public class ServerConfigApplicationTest {

    @Test
    public void testMain() {

    }
}