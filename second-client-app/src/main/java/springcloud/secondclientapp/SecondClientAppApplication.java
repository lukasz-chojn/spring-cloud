package springcloud.secondclientapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SecondClientAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecondClientAppApplication.class, args);
    }

}
