package springcloud.secondclientapp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @Value("${second.client.app.welcome.message}")
    private String message;
    @Value("${server.port}")
    private String serverPort;

    @GetMapping("/")
    public String getMessage() {
        return message +
                " . Application started on port " +
                serverPort;
    }
}
