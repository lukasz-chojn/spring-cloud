package springcloud.apigateway;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @Value("${api.gateway.welcome.message}")
    private String message;

    @GetMapping("/")
    public String getMessage() {
        return message;
    }
}
