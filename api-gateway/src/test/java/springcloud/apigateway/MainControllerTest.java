package springcloud.apigateway;

import org.mockito.InjectMocks;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.testng.Assert.assertEquals;

public class MainControllerTest {

    private MockMvc mockMvc;
    @InjectMocks
    private MainController mainController;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
    }

    @Test
    public void testGetMessage() throws Exception {
        ReflectionTestUtils.setField(mainController, "message", "Hello");

        MockHttpServletRequestBuilder requestBuilder = get("/");

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
    }
}