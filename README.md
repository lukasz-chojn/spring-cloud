# Spring cloud example app

Spring cloud app is an application written for educational purposes.
It includes the following components:
* Spring Cloud Config Server
* Spring Eureka Server
* Spring Zuul Proxy
* Additionally, two web applications that work with a proxy have been added.

Each application reports its presence to the Eureka server and displays a welcome message on the main endpoint (except for the Eureka Server where the welcome endpoint is /hello. On the main endpoint there is a GUI that displays basic information about the application).